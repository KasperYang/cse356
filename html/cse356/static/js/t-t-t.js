var arr = [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '];
function refreshGrid(winner){
    for(int i = 0; i < 8; i++){
        var n = i.toString();
        if(arr[i] != ' '){
            document.getElementById(n).disabled = true;
            document.getElementById(n).innerHTML = arr[i];
        } else {
            document.getElementById(n).disabled = false;
            document.getElementById(n).innerHTML = arr[i];
        }
    }
    if(winner != ' '){
        for(int i = 0; i < 9; i++){
            var n = i.toString();
            document.getElementById(n).disabled = true;
        }
    }
}
document.getElementById("0").onclick = function () {
    this.disabled = true;
    this.innerHTML = 'X';
    arr[0] = 'X';
    console.log(arr[0]);
    $.ajax({
        method: 'POST',
        url: '/ttt/play',
        contentType: 'application/json',
        data: JSON.stringify({ "grid": arr }),
        success : function(data){
            arr = data.grid
            refreshGrid(data.winner);
            refreshWinnder(data.winner);
            if(data.error){
                // display the error message in the login-error div
                $('#login-error').text(data.error);
            }else{
                // redirect to home page
                window.location.href = '/';
            }
        }
    });
}
document.getElementById("1").onclick = function () {
    this.disabled = true;
    this.innerHTML = 'X';
    arr[1] = 'X';
    console.log(arr);
}
document.getElementById("2").onclick = function () {
    this.disabled = true;
    this.innerHTML = 'X';
    arr[2] = 'X';
    console.log(arr);
}
document.getElementById("3").onclick = function () {
    this.disabled = true;
    this.innerHTML = 'X';
    arr[3] = 'X';
    console.log(arr);
}
document.getElementById("4").onclick = function () {
    this.disabled = true;
    this.innerHTML = 'X';
    arr[4] = 'X';
    console.log(arr);
}
document.getElementById("5").onclick = function () {
    this.disabled = true;
    this.innerHTML = 'X';
    arr[5] = 'X';
    console.log(arr);
}
document.getElementById("6").onclick = function () {
    this.disabled = true;
    this.innerHTML = 'X';
    arr[6] = 'X';
    console.log(arr);
}
document.getElementById("7").onclick = function () {
    this.disabled = true;
    this.innerHTML = 'X';
    arr[7] = 'X';
    console.log(arr);
}
document.getElementById("8").onclick = function () {
    this.disabled = true;
    this.innerHTML = 'X';
    arr[8] = 'X';
    console.log(arr);
}

//$(document).ready(function(){
//    $('#login-form').on('submit', function(e){
//        username = $('#name-field').val();
//        password = $('#pass-field').val();

//        e.preventDefault();
//        $.ajax({
//            method: 'POST',
//            url: '/login/',
//            contentType: 'application/json',
//            data: JSON.stringify({ "username":username, "password": password}),
//            success : function(data){
//                if(data.error){
//                    // display the error message in the login-error div
//                    $('#login-error').text(data.error);
//                }else{
//                    // redirect to home page
//                    window.location.href = '/';
//                }
//            }
//        });
//    });
//});