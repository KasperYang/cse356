from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
from .forms import Form
import datetime

def index(request):
	if request.method == 'POST':
		form = Form(request.POST)
		if form.is_valid():
			username = request.POST['name']
			curr = datetime.datetime.now()
			time = str(curr.year) + '-' + str(curr.month) + '-' + str(curr.day) + ' ' + str(curr.hour) + ':' + str(curr.minute) + ':'+ str(curr.second)
			context = {'name': username, 'date': time}
			return render(request, 't-t-t.html', context)

	else:
		form = Form()
		context = {'form': form}
		return render(request, 'wp1.html', context)

